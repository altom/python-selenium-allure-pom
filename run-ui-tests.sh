#!/bin/bash
set -eu

if
    test ${#} -eq 1
then
    echo -n "ERROR: incorrect paramters "
    echo "Run with 'chrome' or 'firefox' as first argument to specify browser, and with 'integration' or 'quality' to specify environment"
    exit 1
fi

export UI_TESTS_BROWSER_NAME="${1:-}"
export UI_TESTS_RUN_CONFIG="${2:-}"

echo "Waiting for ${UI_TESTS_BROWSER_NAME} node to appear"
while ! curl -sL "http://selenium-hub:4444/grid/console" |
	grep -qi "${UI_TESTS_BROWSER_NAME}"
do
    echo -n "."
    sleep 0.2
done
echo "ready"

pytest -s -v tests/ -n 5 --reruns 2 --alluredir=allure-results
