FROM python:3.7

WORKDIR /e2e_tests
ADD requirements.txt /e2e_tests/requirements.txt
RUN pip install -r requirements.txt
ADD . /e2e_tests
ENTRYPOINT ["tail", "-f", "/dev/null"]
