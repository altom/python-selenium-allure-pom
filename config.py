import os

tests_config = {
    "integration": {
        "base_url": "https://seleniumtravelstravel.wordpress.com/",
        "admin": {
            "username": "seleniumtravels",
            "password": "seleniumtravles"
        },
        "implicit_wait": "10",
        "screenshot": "on"
    },
    "dev": {
        "base_url": "https://seleniumtravelstravel.dev.wordpress.com/",
        "admin": {
            "username": "seleniumtravels-dev",
            "password": "seleniumtravles-dev"
        },
        "implicit_wait": "10",
        "screeshot": "on"
    }
}

def get(property_name):
    config_name = os.getenv('UI_TESTS_RUN_CONFIG', 'integration')
    return tests_config[config_name][property_name]