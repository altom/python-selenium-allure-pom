#!/bin/bash
set -eu

if
    test ${#} -eq 1
then
    echo -n "ERROR: incorrect paramters "
    echo "Run with 'local_chrome' or 'local_firefox' as first argument to specify browser, and with 'integration' or 'quality' to specify environment"
    exit 1
fi

rm -rf allure-results

export UI_TESTS_BROWSER_NAME="${1:-}"
export UI_TESTS_RUN_CONFIG="${2:-}"

pytest -s -v tests/ --reruns 2 --alluredir=allure-results

