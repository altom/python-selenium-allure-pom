import allure
from assertpy import assert_that
import config
from tests.base_test import TestBase


@allure.feature("UI")
@allure.feature("Login")
@allure.feature("Login Correct")
@allure.title("Test correct login with Admin user")
class TestLoginCorrect(TestBase):
    @allure.title("Test that login page is displayed when opening main URL")
    def test_login_page_displayed(self):
        self.login_page.open()
        assert_that(self.login_page.is_displayed)

    @allure.title("Test that login with correct credentials works")
    def test_login_with_correct_credentials(self):
        self.login_page.open()

        self.login_page.login_with_correct_credentials(
            config.get("admin")["username"],
            config.get("admin")["password"]
        )
        self.wp_admin_page.wait_to_load()
        assert_that(self.login_page.is_displayed).is_false()


@allure.feature("UI")
@allure.feature("Login")
@allure.feature("Login Incorrect")
@allure.title("Test Login with incorrect credentials")
class TestLoginIncorrect(TestBase):
    @allure.title("Test login with incorrect password")
    def test_login_with_incorrect_password(self):
        self.login_page.open()
        self.login_page.login_with_incorrect_password(config.get("admin")["username"], "incorrectpassword")
        self.login_page.wait_to_load()
