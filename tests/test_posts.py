import allure
from assertpy import assert_that
import config
from pages.login_page import LoginPage
from pages.posts_page import PostsPage
from pages.wp_admin_page import WPAdminPage
from tests.base_test import TestBase


@allure.feature("UI")
@allure.feature("Posts")
@allure.title("Test CRUD operations for Posts")
class TestPostsPage(TestBase):

    @classmethod
    def setUpClass(cls):
        super(TestPostsPage, cls).setUpClass()
        cls.login_page.open()
        assert_that(cls.login_page.is_displayed)
        cls.login_page.login_with_correct_credentials(
            config.get("admin")["username"],
            config.get("admin")["password"]
        )
        cls.wp_admin_page.wait_to_load()

    @allure.title("Test view all Posts")
    def test_view_all_posts(self):
        self.posts_page.open()
        all_posts = self.posts_page.get_all_post_items()
        for post in all_posts:
            print(post.get_title())
            post.wait_for_region_to_load()
        assert_that(len(all_posts)).is_greater_than(0)
