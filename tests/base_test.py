import unittest
import os
import json
from selenium import webdriver
from pages.login_page import LoginPage
from pages.posts_page import PostsPage
from pages.wp_admin_page import WPAdminPage
import config
import allure
from allure_commons.types import AttachmentType

class TestBase(unittest.TestCase):

    browser_name = os.getenv('UI_TESTS_BROWSER_NAME', 'local_chrome')
    driver = None

    @classmethod
    def setUpClass(cls):
        cls.base_url = config.get("base_url")
        if cls.browser_name == 'chrome':
            cls.driver = webdriver.Remote(
                command_executor='http://selenium-hub:4444/wd/hub',
                desired_capabilities={ "browserName": "chrome" }
            )
        elif cls.browser_name == 'firefox':
            cls.driver = webdriver.Remote(
                command_executor='http://selenium-hub:4444/wd/hub',
                desired_capabilities={ "browserName": "firefox" }
            )
        elif cls.browser_name == 'local_chrome':
            cls.driver = webdriver.Chrome()
            options = webdriver.ChromeOptions()
            options.add_argument("start-maximized");
            options.add_argument("disable-infobars")
            options.add_argument("--disable-extensions")

        elif cls.browser_name == 'local_firefox':
            cls.driver = webdriver.Firefox()
            options = webdriver.FirefoxOptions()
            options.add_argument("start-maximized");

        cls.driver.implicitly_wait(config.get("implicit_wait"))

        cls.login_page = LoginPage(cls.driver, cls.base_url)
        cls.wp_admin_page = WPAdminPage(cls.driver, cls.base_url)
        cls.posts_page = PostsPage(cls.driver, cls.base_url)

    def setUp(self):
        allure.attach(self.browser_name,
                      name="BROWSER CONFIG",
                      attachment_type=AttachmentType.TEXT)

    def tearDown(self):
        allure.attach(self.driver.get_screenshot_as_png(),
                      name='test-end-screenshot',
                      attachment_type=AttachmentType.PNG)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()