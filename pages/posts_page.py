import allure
from pypom import Region
from selenium.webdriver.common.by import By
from pages.wp_admin_page import WPAdminPage


class PostsPage(WPAdminPage):
    URL_TEMPLATE = '/wp-admin/edit.php'

    _title = (By.CLASS_NAME, 'wp-heading-inline')
    _posts_list = (By.ID, 'the-list')
    _add_new_button = (By.XPATH, '//div[@class="wrap"]/a[contains(@href, "wp-admin/post-new.php")]')

    @staticmethod
    def _post_by_index(index):
        return By.XPATH, '//tbody[@id="the-list"]/tr[' + str(index) + ']'

    def __init__(self, driver, base_url, **url_kwargs):
        super(PostsPage, self).__init__(driver, base_url, **url_kwargs)

    @allure.step("[ Posts Page ] - Wait to load")
    def wait_to_load(self):
        super(PostsPage, self).wait_to_load()
        self.wait_until_element_is_displayed(self._add_new_button)
        self.driver.find_element(*self._title)
        self.driver.find_element(*self._posts_list)
        self.screenshot("posts_page_loaded")

    @property
    @allure.step("[ Posts Page ] - Check if it's displayed")
    def is_displayed(self):
        return self.is_element_displayed(self._posts_list)

    @allure.step("[ Posts Page ] - Get Post Item By Title")
    def get_post_item_by_title(self, title):
        for post in self.get_all_post_items():
            if post.get_title() == title:
                return post
        return None

    @allure.step("[ Posts Page ] - Get All Post Items")
    def get_all_post_items(self):
        posts = self.driver.find_element(*self._posts_list).find_elements(By.XPATH, 'tr')
        post_items = []
        for index in range(1, len(posts)):
            post_item = self.PostItemInList(page=self, index=index)
            post_items.append(post_item)
        return post_items

    class PostItemInList(Region):
        _title_locator = (By.CLASS_NAME, 'title')
        _author_locator = (By.CLASS_NAME, 'author')
        _categories_locator = (By.CLASS_NAME, 'categories')
        _tags_locator = (By.CLASS_NAME, 'tags')
        _comments_locator = (By.CLASS_NAME, 'comments')
        _likes_locator = (By.CLASS_NAME, 'likes')
        _date_locator = (By.CLASS_NAME, 'date')

        _title_text_locator = (By.XPATH, 'td/strong/a[@class="row-title"]')

        def __init__(self, page, index):
            super().__init__(page)
            self._root_locator = PostsPage._post_by_index(index)

        @property
        @allure.step("[ Post Item in List ] - Check if it's displayed")
        def is_displayed(self):
            return self.page.is_element_displayed(self._title_locator)

        @allure.step("[ Post Item in List ] - Wait to load")
        def wait_for_region_to_load(self):
            self.find_element(*self._title_locator)
            self.find_element(*self._author_locator)
            self.find_element(*self._categories_locator)
            self.find_element(*self._tags_locator)
            self.find_element(*self._comments_locator)
            self.find_element(*self._likes_locator)
            self.find_element(*self._date_locator)

        @allure.step("[ Post Item in List ] - Get title")
        def get_title(self):
            title = self.find_element(*self._title_text_locator).text
            return title
