import allure
from selenium.webdriver.common.by import By
from pages.base_page import BasePage


class WPAdminPage(BasePage):
    URL_TEMPLATE = '/wp-admin'

    _top_bar = (By.ID, 'wpadminbar')
    _menu_container = (By.ID, 'adminmenu')
    _menu_dashboard = (By.ID, 'menu-dashboard')
    _menu_posts= (By.ID, 'menu-posts')
    _menu_media = (By.ID, 'menu-media')
    _menu_links = (By.ID, 'menu-links')
    _menu_pages = (By.ID, 'menu-pages')
    _menu_comments = (By.ID, 'menu-comments')
    _menu_tools = (By.ID, 'menu-tools')

    def __init__(self, driver, base_url, **url_kwargs):
        super(WPAdminPage, self).__init__(driver, base_url, **url_kwargs)

    @allure.step("[ WPAdmin Page Menu ] - Wait to load")
    def wait_to_load(self):
        self.wait_until_element_is_displayed(self._menu_container)

        self.driver.find_element(*self._top_bar)
        self.driver.find_element(*self._menu_dashboard)
        self.driver.find_element(*self._menu_posts)
        self.driver.find_element(*self._menu_media)
        self.driver.find_element(*self._menu_links)
        self.driver.find_element(*self._menu_pages)
        self.driver.find_element(*self._menu_comments)
        self.driver.find_element(*self._menu_tools)
        self.screenshot("wp_admin_page_loaded")

    @property
    @allure.step("[ WPAdmin Page Menu ] - Check if it's displayed")
    def is_displayed(self):
        return self.is_element_displayed(self._menu_container)

    @allure.step("[ WPAdmin Page Menu ] - Go to Dashboard")
    def go_to_dashboard(self):
        self.click_element(self._menu_dashboard)

    @allure.step("[ WPAdmin Page Menu ] - Go to Posts")
    def go_to_posts(self):
        self.click_element(self._menu_posts)

    @allure.step("[ WPAdmin Page Menu ] - Go to Media")
    def go_to_media(self):
        self.click_element(self._menu_media)

    @allure.step("[ WPAdmin Page Menu ] - Go to Links")
    def go_to_links(self):
        self.click_element(self._menu_links)

    @allure.step("[ WPAdmin Page Menu ] - Go to Pages")
    def go_to_pages(self):
        self.click_element(self._menu_pages)

    @allure.step("[ WPAdmin Page Menu ] - Go to Comments")
    def go_to_comments(self):
        self.click_element(self._menu_comments)

    @allure.step("[ WPAdmin Page Menu ] - Go to Tools")
    def go_to_tools(self):
        self.click_element(self._menu_tools)


