import allure
from selenium.webdriver.common.by import By
from pages.base_page import BasePage


class LoginPage(BasePage):
    URL_TEMPLATE = '/wp-admin'

    _logo = (By.CLASS_NAME, 'masterbar__wpcom-logo')
    _lost_your_password = (By.PARTIAL_LINK_TEXT, 'Lost your password?')
    _username_input = (By.ID, 'usernameOrEmail')
    _password_input = (By.ID, 'password')
    _login_button = (By.XPATH, '//div[@class="login__form-action"]/button')
    _incorrect_login_text_locator = (By.CLASS_NAME, '//div[@class="form-input-validation is-error"]')

    def __init__(self, driver, base_url, incorrect_login=False, **url_kwargs):
        super(LoginPage, self).__init__(driver, base_url, **url_kwargs)

    @allure.step("[ Login Page ] - Wait to load")
    def wait_to_load(self):
        self.wait_until_element_is_displayed(self._login_button)
        self.driver.find_element(*self._logo)
        self.driver.find_element(*self._username_input)
        self.driver.find_element(*self._lost_your_password)
        self.screenshot("login_page_loaded")

    @property
    @allure.step("[ Login Page ] - Check if it's displayed")
    def is_displayed(self):
        return self.is_element_displayed(self._login_button)

    def set_login_username(self, username):
        self.set_value(self._username_input, username)
        self.screenshot("set_username")

    def set_login_password(self, password):
        self.set_value(self._password_input, password)
        self.screenshot("set_password")

    @allure.step("[ Login Page ] - Login with incorrect username")
    def login_with_incorrect_username(self, username):
        self.set_login_username(username)
        self.click_element(self._login_button)
        self.screenshot("incorrect_username")

    @allure.step("[ Login Page ] - Login with incorrect password")
    def login_with_incorrect_password(self, username, password):
        self.set_login_username(username)
        self.click_element(self._login_button)
        self.set_login_password(password)
        self.click_element(self._login_button)
        self.screenshot("incorrect_password")

    @allure.step("[ Login Page ] - Login with correct credentials")
    def login_with_correct_credentials(self, username, password):
        self.set_login_username(username)
        self.click_element(self._login_button)
        self.set_login_password(password)
        self.click_element(self._login_button)
        self.screenshot("correct_login")
