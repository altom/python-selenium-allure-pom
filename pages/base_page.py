import time
import allure
from allure_commons.types import AttachmentType
from pypom import Page
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
import config


class BasePage(Page):

    def __init__(self, driver, base_url="", **url_kwargs):
        super(BasePage, self).__init__(driver, base_url, **url_kwargs)

    @property
    def page_title(self):
        return self.wait.until(lambda s: self.driver.title)

    def open(self):
        super(BasePage, self).open()
        self.wait_to_load()

    def wait_to_load(self):
        return self

    @allure.step("Set value")
    def set_value(self, locator, value):
        element = self.driver.find_element(*locator)
        WebDriverWait(self.driver, 10).until(expected_conditions.element_to_be_clickable(locator))
        if element.get_attribute("value") != "":
            element.clear()
        element.send_keys(value)
        retries = 0
        while element.get_attribute("value") != value and retries < 10:
            retries += 1
            self.clear_value(locator)
            element.send_keys(value)
        self.screenshot("after_set_value_to_element")

    @allure.step("Clear value")
    def clear_value(self, locator):
        element = self.driver.find_element(*locator)
        while element.get_attribute("value") != '':
            element.send_keys(Keys.BACK_SPACE)
        self.screenshot("after_clear_value_from_element")

    @allure.step("Click element")
    def click_element(self, locator):
        WebDriverWait(self.driver, 10).until(expected_conditions.element_to_be_clickable(locator))
        self.find_element(*locator).click()
        self.screenshot("after_click_element")

    @allure.step("Set checkbox to {selected}")
    def set_checkbox(self, xpath, selected):
        checkbox = self.find_element(By.XPATH, xpath + "//input")
        if (selected and not checkbox.is_selected()) or (not selected and checkbox.is_selected()):
            retries = 0
            while (checkbox.is_selected() is not selected) and retries < 10:
                self.click_element((By.XPATH, xpath + "//label"))
                retries += 1
                time.sleep(1)
        if (selected and not checkbox.is_selected()) or (not selected and checkbox.is_selected()):
            raise Exception("Checkbox with xpath " + str(xpath) + " could not be set to " + str(selected))
        self.screenshot("after_setting_checkbox_to_" + str(selected))

    @allure.step("Check if checkbox is selected")
    def is_checkbox_selected(self, xpath):
        return self.find_element(By.XPATH, xpath + "//input").is_selected()

    @allure.step("Check if element is displayed")
    def is_element_displayed(self, locator):
        self.driver.implicitly_wait(0)
        displayed = False
        try:
            self.driver.find_element(*locator)
            displayed = True
        except Exception as e:
            displayed = False
        finally:
            self.driver.implicitly_wait(config.get("implicit_wait"))
            self.screenshot("after_checking_if_element_is_displayed")
            return displayed

    @allure.step("Wait until element is NOT displayed")
    def wait_until_element_is_not_displayed(self, locator, timeout=10, fail_if_found=True):
        waited = 0
        while waited < timeout and self.is_element_displayed(locator):
            time.sleep(0.5)
            waited += 0.5
        self.screenshot("after_waiting_for_element_to_not_be_displayed")
        if fail_if_found and self.is_element_displayed(locator):
            raise Exception("Element " + str(locator) + " was visible and shouldn't have been")

    @allure.step("Wait until element is displayed")
    def wait_until_element_is_displayed(self, locator, timeout=10, fail_if_not_found=True):
        waited = 0
        while waited < timeout and not self.is_element_displayed(locator):
            time.sleep(0.5)
            waited += 0.5
        self.screenshot("after_waiting_for_element_to_be_displayed")
        if fail_if_not_found and not self.is_element_displayed(locator):
            raise Exception("Element " + str(locator) + " was not visible and should have been")

    def screenshot(self, name):
        if config.get("screenshot") is "on":
            allure.attach(self.driver.get_screenshot_as_png(), name=name, attachment_type=AttachmentType.PNG)
