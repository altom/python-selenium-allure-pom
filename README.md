# Pytest-Selenium-Allure-POM-Example

This project can be used as a starting point for writing Selenium tests using `pytest` and Allure in Python. 
The tests are written following the Page Object Model. 

The tests use a Wordpress.com blog (https://seleniumtravelstravel.wordpress.com/) as the Application Under Test. 


### Setup environment

1. If you don't already have a virtual environemnt, run:

```
virtualenv -p python3 .venv-e2e
source .venv-e2e/bin/activate
```

2. Install all the dependencies by running `pip install -r requirement.txt`


### Running the UI tests

The tests use the following environment variables to set the browser to run on and the environment to run against (with the default values as shown below):
```
UI_TESTS_BROWSER_NAME=local_chrome
UI_TESTS_RUN_CONFIG=integration
```

The run configurations are defined in `config.py`

For browsers, the following have been configured:
- `chrome` and `firefox` will try to run the tests against a Selenium Hub running in Docker, as described in the section below. 
- `local_chrome` and `local_firefox` will try to run the browsers on the local machine

#### Using Docker

To run the tests, run the following commands:

1. Run `docker-compose -f docker-compose.local.yml up --build -d` to run Selenium Hub and the 2 nodes for Chrome and Firefox
2. Edit the `run-tests.sh` file and set the `UI_TESTS_RUN_CONFIG` to the environemnt you want to run the tests against. For example, set `UI_TESTS_RUN_CONFIG=dev` to run them against `dev`. The default will run against `integration`
3. Run `./run-tests.sh chrome` or `./run-tests.sh firefox` to run the tests on either browser


#### Using local browsers

Use the following command to run the tests and collect allure results:
`pytest -s -v tests/ --alluredir=allure-results`

You can also use the `run-ui-tests-locally.sh` file to run all the tests.

### Viewing the Allure Results

To view the Allure results, first install the Allure CLI:
https://docs.qameta.io/allure/#_installing_a_commandline

Once you have that installed, just run:
`allure serve ./allure-results`